<?php
/**
 * GSC Tesseract
 * php version 8.2
 *
 * @category CMS
 * @package  Framework
 * @author   Fred Brooker <git@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 * @link     https://app.gscloud.cz
 */

namespace GSC;

use Cake\Cache\Cache;
use League\Csv\Reader;
use League\Csv\Statement;
use RedisClient\RedisClient;

/**
 * API Presenter
 * 
 * @category CMS
 * @package  Framework
 * @author   Fred Brooker <git@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 * @link     https://app.gscloud.cz
 */
class ApiPresenter extends APresenter
{
    const MAX_API_HITS = 1000;
    const ACCESS_TIME_LIMIT = 3599;
    const EVENT_HEADER = "DATE,REG_URI,TITLE_CZ,TITLE_EN,FEE,DIS,TYPE,START,"
        . "END,DETAIL_CZ,DETAIL_EN,LOC,GARANT,ORG,URI";
    const MENU_HEADER_DAYILY = "DAY,TYPE,DATE,TITLE_CZ,ALG,ZAP";
    const MENU_HEADER_WEEKLY = "TYPE,VOL,FLAG,KOD,STAR,TITLE,PRICE,PRICE_ORD,"
        . "SERVING,ZAP,ZAP_AC,ALG";
    const MENU_HEADER_CATERING = "TYPE,VOL,FLAG,TITLE,PRICE,ZAP,ALG";
    const API_CACHE = "tenminutes";

    /**
     * Main controller
     * 
     * @param mixed $param optional parameter
     * 
     * @return object the controller
     */
    public function process($param = null)
    {
        $view = $this->getView();
        setlocale(LC_ALL, "cs_CZ.utf8");
        $use_cache = true;
        //$use_cache = false;

        // general API properties
        $extras = [
            "api_quota" => (int) self::MAX_API_HITS,
            "api_usage" => $this->accessLimiter(),
            "uuid" => $this->getUID(),
            "access_time_limit" => self::ACCESS_TIME_LIMIT,
            "cache_time_limit" => $this->getData("cache_profiles")[self::API_CACHE],
            "fn" => $view,
            "name" => "Bistro ANDINI",
        ];

        // API calls
        switch ($view) {
        case "GetCateringMenu":
            if ($use_cache && $cache = Cache::read($view, self::API_CACHE)) {
                return $this->writeJsonData($cache, $extras);
            }
            $this->preloadAppData();
            $data = $this->readMenuCatering();
            Cache::write($view, $data, self::API_CACHE);
            return $this->writeJsonData($data, $extras);
                break;

        case "GetWeeklyMenu":
        case "GetLunchMenuNew":
            if ($use_cache && $cache = Cache::read($view, self::API_CACHE)) {
                return $this->writeJsonData($cache, $extras);
            }
            $this->preloadAppData();
            $data = $this->readMenuWeekly();
            Cache::write($view, $data, self::API_CACHE);
            return $this->writeJsonData($data, $extras);
                break;

        case "GetDailyMenu":
        case "GetLunchMenu":
            if ($use_cache && $cache = Cache::read($view, self::API_CACHE)) {
                return $this->writeJsonData($cache, $extras);
            }
            $this->preloadAppData();
            $data = $this->readMenuDaily();
            Cache::write($view, $data, self::API_CACHE);
            return $this->writeJsonData($data, $extras);
                break;

        case "GetEvents":
            if ($use_cache && $cache = Cache::read($view, self::API_CACHE)) {
                return $this->writeJsonData($cache, $extras);
            }
            $this->preloadAppData();
            $data = $this->readEvents();
            Cache::write($view, $data, self::API_CACHE);
            return $this->writeJsonData($data, $extras);
                break;

        default:
            return ErrorPresenter::getInstance()->process(404);
        }

        return $this;
    }

    /**
     * Read menu - daily *** DEPRECATED
     *
     * @return array menu records
     */
    public function readMenuDaily()
    {
        $base58 = new \Tuupola\Base58;
        if (\is_null($csv = $this->readAppData("menu"))) {
            return null;
        }
        $columns = explode(',', self::MENU_HEADER_DAILY);
        foreach ($columns as $col) {
            $$col = [];
            try {
                $reader = Reader::createFromString($csv);
                $reader->setHeaderOffset(0);
                $records = (new Statement())->offset(1)->process($reader);
                $i = 0;
                foreach ($records->fetchColumn($col) as $x) {
                    $$col[$i] = $x;
                    $i++;
                }
            } catch (Exception $e) {
                $this->addError("EXCEPTION: app MENU reader, column: $col");
                return $this;
            }
        }

        /**
         * Reformat alergens
         *
         * @param string $x string
         * 
         * @return string
         */
        function reformatAlg($x)
        {
            return (!$x) ? "" : "{$x}";
        }

        $arr = [];
        for ($i = 0; $i < count($DAY); $i++) {
            if (!strlen(trim($ZAP[$i]))) {
                continue;
            }
            $TITLE_CZ[$i] = $TITLE_CZ[$i] ?? "";
            StringFilters::correctTextSpacing($TITLE_CZ[$i], "cs");
            $arr[] = [
                "id" => $base58->encode(
                    hash(
                        "sha256", $DAY[$i] . $DATE[$i] . $TITLE_CZ[$i]
                    )
                ),
                "date" => $DATE[$i] ?? "",
                "dayOfWeek" => $DAY[$i] ?? 0,
                "menuType" => strtoupper(substr($TYPE[$i], 0, 1)) ?? "X",
                "titleCs" => $TITLE_CZ[$i],
                "allergens" => reformatAlg($ALG[$i] ?? ""),
            ];
        }
        return $arr;
    }

    /**
     * Read events
     *
     * @return array event records
     */
    public function readEvents()
    {
        $base58 = new \Tuupola\Base58;
        if (\is_null($csv = $this->readAppData("akce"))) {
            return null;
        }
        $arr = [];
        $columns = explode(',', self::EVENT_HEADER);
        foreach ($columns as $col) {
            $$col = [];
            try {
                $reader = Reader::createFromString($csv);
                $reader->setHeaderOffset(0);
                $records = (new Statement())->offset(1)->process($reader);
                $i = 0;
                foreach ($records->fetchColumn($col) as $x) {
                    $$col[$i] = $x;
                    $i++;
                }
            } catch (Exception $e) {
                $this->addError("EXCEPTION: app EVENT reader, column: $col");
                return $this;
            }
        }

        /**
         * Reformat date
         * 
         * @param string $x date string
         * 
         * @return string
         */
        function reformatDate($x)
        {
            if (!$x) {
                return "";
            }
            $x = explode('.', $x);
            if (count($x) == 3) {
                if (strlen($x[2]) == 4) {
                    if (strlen($x[0]) < 2) {
                        $x[0] = "0{$x[0]}";
                    }
                    if (strlen($x[1]) < 2) {
                        $x[1] = "0{$x[1]}";
                    }
                    return "{$x[0]}.{$x[1]}.{$x[2]}";
                }
            }
            return "";
        }

        for ($i = 0; $i < count($DATE); $i++) {
            if (strlen(trim($DIS[$i]))) {
                continue;
            }
            if (!strlen(trim($TITLE_CZ[$i]))) {
                continue;
            }
            $TITLE_CZ[$i] = $TITLE_CZ[$i] ?? "";
            $TITLE_EN[$i] = $TITLE_EN[$i] ?? "";
            $DETAIL_CZ[$i] = $DETAIL_CZ[$i] ?? "";
            $DETAIL_EN[$i] = $DETAIL_EN[$i] ?? "";
            StringFilters::correctTextSpacing($TITLE_CZ[$i], "cs");
            StringFilters::correctTextSpacing($TITLE_EN[$i], "en");
            StringFilters::correctTextSpacing($DETAIL_CZ[$i], "cs");
            StringFilters::correctTextSpacing($DETAIL_EN[$i], "en");
            $arr[] = [
                "id" => $base58->encode(hash("sha256", $DATE[$i] . $TITLE_CZ[$i])),
                "date" => reformatDate($DATE[$i]),
                "eventStart" => $START[$i] ?? "",
                "eventEnd" => $END[$i] ?? "",
                "registrationUrl" => $REG_URI[$i] ?? "",
                "titleCs" => $TITLE_CZ[$i],
                "titleEn" => $TITLE_EN[$i],
                "detailCs" => $DETAIL_CZ[$i],
                "detailEn" => $DETAIL_EN[$i],
                "eventFee" => (int) $FEE[$i] ?? 0,
                "code" => $TYPE[$i] ?? "",
                "location" => $LOC[$i] ?? "sál/café",
                "eventHost" => $GARANT[$i] ?? "",
                "orgName" => $ORG[$i] ?? "",
                "orgUrl" => $URI[$i] ?? "",
            ];
        }
        return $arr;
    }

    /**
     * Read menu - weekly
     *
     * @return array menu records
     */
    public function readMenuWeekly()
    {
        $base58 = new \Tuupola\Base58;
        if (\is_null($csv = $this->readAppData("weekly"))) {
            return null;
        }
        $columns = explode(',', self::MENU_HEADER_WEEKLY);
        foreach ($columns as $col) {
            $$col = [];
            try {
                $reader = Reader::createFromString($csv);
                $reader->setHeaderOffset(0);
                $records = (new Statement())->offset(1)->process($reader);
                $i = 0;
                foreach ($records->fetchColumn($col) as $x) {
                    $$col[$i] = $x;
                    $i++;
                }
            } catch (Exception $e) {
                $this->addError("EXCEPTION: app NEW MENU reader, column: $col");
                return $this;
            }
        }

        $arr = [];
        for ($i = 0; $i < count($TYPE); $i++) {
            if (!strlen(trim($ZAP[$i]))) {
                continue;
            }
            if (!strlen(trim($TYPE[$i]))) {
                continue;
            }
            if (!strlen(trim($TITLE[$i]))) {
                continue;
            }
            if (!(int) trim($PRICE[$i])) {
                continue;
            }

            // parse TITLE
            StringFilters::correctTextSpacing($TITLE[$i], "cs");

            // parse PRICE
            $x = $PRICE[$i] ?? "";
            preg_match('!(\d+)!', $x, $matches);
            $PRICE[$i] = $matches[1] ?? 0;
            if (!$PRICE[$i]) {
                continue;
            }

            // parse PRICE_ORD
            $x = $PRICE_ORD[$i] ?? "";
            preg_match('!(\d+)!', $x, $matches);
            $PRICE_ORD[$i] = $matches[1] ?? 0;

            // parse FLAGS
            $x = $FLAG[$i] ?? "";
            $FLAG_RAW[$i] = $x; // FLAG backup

            //VG = vegan
            //LC = low carb
            $x = \str_replace("VG", "🍀 ", $x);
            $x = \str_replace("LC", "🍭 ", $x);

            //BL = bez lepku
            //BM = bez mléka
            //BV = bez vajec
            $x = \str_replace("BL", "bez&nbsp;lepku ", $x);
            $x = \str_replace("BM", "bez&nbsp;mléka ", $x);
            $x = \str_replace("BV", "bez&nbsp;vajec ", $x);

            //KM = kuřecí maso
            //VM = vepřové maso
            //HV = hovězí maso
            //RY = ryba
            //VJ = vejce
            //SY = sýr
            //ML = mléko
            //CH = chilli
            $x = \str_replace("KM", "🐔 ", $x);
            $x = \str_replace("VM", "🐷 ", $x);
            $x = \str_replace("HV", "🐮 ", $x);
            $x = \str_replace("RY", "🐟 ", $x);
            $x = \str_replace("VJ", "🥚 ", $x);
            $x = \str_replace("SY", "🧀 ", $x);
            $x = \str_replace("ML", "🥛 ", $x);
            $x = \str_replace("CH", "🌶 ", $x);
            $FLAG[$i] = $x;

            // fix FLAGS in ENGLISH
            $x = \str_replace("volitelně", "optional", $x);
            $x = \str_replace("bez&nbsp;lepku", "gluten-free", $x);
            $x = \str_replace("bez&nbsp;mléka", "no&nbsp;milk", $x);
            $x = \str_replace("bez&nbsp;vajec", "no&nbsp;eggs", $x);
            $FLAG_EN[$i] = $x;

            // fix VOL in ENGLISH
            $x = $VOL[$i] ?? "";
            $VOL_RAW[$i] = $x; // RAW backup
            $x = \str_replace("1 ks", "1 pc", $x);
            $x = \str_replace(" ks", " pcs", $x);
            $VOL_EN[$i] = $x;

            // add non-breakable spaces to VOLs
            $VOL[$i] = \str_replace(" ", "&nbsp;", $VOL[$i]);
            $VOL_EN[$i] = \str_replace(" ", "&nbsp;", $VOL_EN[$i]);

            // parse ALG
            $x = explode(" ", strtr(trim($ALG[$i]), ",;:-", "    "));
            $x = array_filter($x, 'strlen');
            $x = array_unique($x);
            $ALG[$i] = \implode(", ", $x);

            // export array
            $arr[] = [
                "id" => $base58->encode(
                    hash(
                        "sha256", $TITLE[$i] . $VOL[$i] . $PRICE[$i]
                    )
                ),
                "star" => strlen($STAR[$i]) ? true : false,
                "type" => strtoupper($TYPE[$i]),
                "title" => $TITLE[$i],
                "volume" => $VOL[$i],
                "volume_en" => $VOL_EN[$i],
                "volume_raw" => $VOL_RAW[$i],
                "flags" => $FLAG[$i] ?? "",
                "flags_en" => $FLAG_EN[$i] ?? "",
                "flags_raw" => $FLAG_RAW[$i] ?? "",
                "kod" => $KOD[$i] ?? "",
                "price" => $PRICE[$i] ?? "",
                "price_ord" => $PRICE_ORD[$i] ?? "",
                "allergens" => $ALG[$i] ?? "",
            ];
        }
        return $arr;
    }

    /**
     * Read menu - catering
     *
     * @return array menu records
     */
    public function readMenuCatering()
    {
        $base58 = new \Tuupola\Base58;
        if (\is_null($csv = $this->readAppData("catering"))) {
            return null;
        }
        $columns = explode(',', self::MENU_HEADER_CATERING);
        foreach ($columns as $col) {
            $$col = [];
            try {
                $reader = Reader::createFromString($csv);
                $reader->setHeaderOffset(0);
                $records = (new Statement())->offset(1)->process($reader);
                $i = 0;
                foreach ($records->fetchColumn($col) as $x) {
                    $$col[$i] = $x;
                    $i++;
                }
            } catch (Exception $e) {
                $this->addError("EXCEPTION: app CATERING MENU reader, column: $col");
                return $this;
            }
        }

        $arr = [];
        for ($i = 0; $i < count($TYPE); $i++) {
            if (!strlen(trim($ZAP[$i]))) {
                continue;
            }
            if (!strlen(trim($TYPE[$i]))) {
                continue;
            }
            if (!strlen(trim($TITLE[$i]))) {
                continue;
            }
            if (!(int) trim($PRICE[$i])) {
                continue;
            }

            // parse TITLE
            StringFilters::correctTextSpacing($TITLE[$i], "cs");

            // parse PRICE
            $x = $PRICE[$i] ?? "";
            preg_match('!(\d+)!', $x, $matches);
            $PRICE[$i] = $matches[1] ?? 0;
            if (!$PRICE[$i]) {
                continue;
            }

            // parse FLAGS
            $x = $FLAG[$i] ?? "";
            $FLAG_RAW[$i] = $x; // FLAG backup

            //VG = vegan
            //LC = low carb
            $x = \str_replace("VG", "🍀 ", $x);
            $x = \str_replace("LC", "🍭 ", $x);

            //BL = bez lepku
            //BM = bez mléka
            //BV = bez vajec
            $x = \str_replace("BL", "bez&nbsp;lepku ", $x);
            $x = \str_replace("BM", "bez&nbsp;mléka ", $x);
            $x = \str_replace("BV", "bez&nbsp;vajec ", $x);

            //KM = kuřecí maso
            //VM = vepřové maso
            //HV = hovězí maso
            //RY = ryba
            //VJ = vejce
            //SY = sýr
            //ML = mléko
            //CH = chilli
            $x = \str_replace("KM", "🐔 ", $x);
            $x = \str_replace("VM", "🐷 ", $x);
            $x = \str_replace("HV", "🐮 ", $x);
            $x = \str_replace("RY", "🐟 ", $x);
            $x = \str_replace("VJ", "🥚 ", $x);
            $x = \str_replace("SY", "🧀 ", $x);
            $x = \str_replace("ML", "🥛 ", $x);
            $x = \str_replace("CH", "🌶 ", $x);
            $FLAG[$i] = $x;

            // fix FLAGS in ENGLISH
            $x = \str_replace("volitelně", "optional", $x);
            $x = \str_replace("bez&nbsp;lepku", "gluten-free", $x);
            $x = \str_replace("bez&nbsp;mléka", "no&nbsp;milk", $x);
            $x = \str_replace("bez&nbsp;vajec", "no&nbsp;eggs", $x);
            $FLAG_EN[$i] = $x;

            // fix VOL in ENGLISH
            $x = $VOL[$i] ?? "";
            $VOL_RAW[$i] = $x; // RAW backup
            $x = \str_replace("1 ks", "1 pc", $x);
            $x = \str_replace(" ks", " pcs", $x);
            $VOL_EN[$i] = $x;

            // add non-breakable spaces to VOLs
            $VOL[$i] = \str_replace(" ", "&nbsp;", $VOL[$i]);
            $VOL_EN[$i] = \str_replace(" ", "&nbsp;", $VOL_EN[$i]);

            // parse ALG
            $x = explode(" ", strtr(trim($ALG[$i]), ",;:-", "    "));
            $x = array_filter($x, 'strlen');
            $x = array_unique($x);
            $ALG[$i] = \implode(", ", $x);

            // export array
            $arr[] = [
                "id" => $base58->encode(
                    hash(
                        "sha256", $TITLE[$i] . $VOL[$i] . $PRICE[$i]
                    )
                ),
                "type" => strtoupper($TYPE[$i]),
                "title" => $TITLE[$i],
                "volume" => $VOL[$i],
                "volume_en" => $VOL_EN[$i],
                "volume_raw" => $VOL_RAW[$i],
                "flags" => $FLAG[$i] ?? "",
                "flags_en" => $FLAG_EN[$i] ?? "",
                "flags_raw" => $FLAG_RAW[$i] ?? "",
                "price" => $PRICE[$i] ?? "",
                "allergens" => $ALG[$i] ?? "",
            ];
        }
        return $arr;
    }

    /**
     * Redis access limiter
     *
     * @return mixed access count or null
     */
    public function accessLimiter()
    {
        $hour = date('H');
        $uid = $this->getUID();
        defined('SERVER') || define(
            'SERVER',
            strtolower(
                preg_replace(
                    "/[^A-Za-z0-9]/", '', $_SERVER['SERVER_NAME'] ?? 'localhost'
                )
            )
        );
        defined('PROJECT') || define('PROJECT', 'LASAGNA');
        $key = 'access_limiter_' . SERVER . '_' . PROJECT . "_{$hour}_{$uid}";
        \error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
        $redis = new RedisClient(
            [
            'server' => 'localhost:6377',
            'timeout' => 1,
            ]
        );
        try {
            $val = (int) @$redis->get($key);
        } catch (\Exception $e) {
            return null;
        }
        if ($val > self::MAX_API_HITS) {
            // over limit!
            $this->setLocation('/err/420');
        }
        try {
            @$redis->multi();
            @$redis->incr($key);
            @$redis->expire($key, self::ACCESS_TIME_LIMIT);
            @$redis->exec();
        } catch (\Exception $e) {
            return null;
        }
        $val++;
        return $val;
    }
}
