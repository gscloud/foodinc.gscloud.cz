<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'phpseclib3\\' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'chillerlan\\Settings\\' => array($vendorDir . '/chillerlan/php-settings-container/src'),
    'chillerlan\\QRCode\\' => array($vendorDir . '/chillerlan/php-qrcode/src'),
    'Tuupola\\' => array($vendorDir . '/tuupola/base58/src'),
    'Symfony\\Polyfill\\Php80\\' => array($vendorDir . '/symfony/polyfill-php80'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Component\\Lock\\' => array($vendorDir . '/symfony/lock'),
    'Seld\\CliPrompt\\' => array($vendorDir . '/seld/cli-prompt/src'),
    'Psr\\SimpleCache\\' => array($vendorDir . '/psr/simple-cache/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-factory/src', $vendorDir . '/psr/http-message/src'),
    'Psr\\Http\\Client\\' => array($vendorDir . '/psr/http-client/src'),
    'ParagonIE\\HiddenString\\' => array($vendorDir . '/paragonie/hidden-string/src'),
    'ParagonIE\\Halite\\' => array($vendorDir . '/paragonie/halite/src'),
    'ParagonIE\\ConstantTime\\' => array($vendorDir . '/paragonie/constant_time_encoding/src'),
    'Michelf\\' => array($vendorDir . '/michelf/php-markdown/Michelf'),
    'League\\OAuth2\\Client\\' => array($vendorDir . '/league/oauth2-client/src', $vendorDir . '/league/oauth2-google/src'),
    'League\\Csv\\' => array($vendorDir . '/league/csv/src'),
    'League\\CLImate\\' => array($vendorDir . '/league/climate/src'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'GSC\\' => array($baseDir . '/app'),
    'Cloudflare\\API\\' => array($vendorDir . '/cloudflare/sdk/src'),
    'Cake\\Utility\\' => array($vendorDir . '/cakephp/utility'),
    'Cake\\Core\\' => array($vendorDir . '/cakephp/core'),
    'Cake\\Cache\\' => array($vendorDir . '/cakephp/cache'),
    'Adbar\\' => array($vendorDir . '/adbario/php-dot-notation/src'),
);
